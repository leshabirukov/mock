
#include "stdafx.h"
#include "envi_graph.h"

// cell automata logic

void 
VState::doStep( MessageBody& fromParent, 
                MessageBody& fromLeft, MessageBody& fromRight )
{
    if ( fromParent.is_bomb() ){	//  garbage collection
        if ( state!=VMS_EMPTY ){
		    n->message= Message( TO_CHILDREN, CMD_BOMB );
		    n->state= VMS_EMPTY;
		    n->value= VK_EMPTY;
            n->swapRL= false;
        }
	}
    else
    switch ( state ){
	case VMS_EMPTY:	
        //	Sleeping.
        //  Data from parent used for initialization self/children
        //  Works on    1.initial load
        //              2.string application
    {
        if ( value == VK_TRANSIT ){             //  from I0
			n->message= Message( TO_LEFT, fromParent.getk() );
            if ( fromParent.getk() == VK_EOF ){
 			    n->state  = VMS_READY;
                n->step = 1;
            }
        }
		else if ( !value )	
		{	//	writing left
            n->value=  fromParent.getk();
			if ( fromParent.getk() )
			{
                if ( CH_NUM( fromParent.getk() )==0 ){
				    n->message= Message( TO_PARENT, fromParent.getk() );
				    n->state  = VMS_READY;
                }
			}
            else{
                n->message= Message( TO_PARENT, VK_EMPTY );   //  cleaning, not necessary
            }
		}
        else if ( CH_NUM( value )!=0 && ( fromLeft.zero() && step==0 ) )
		{								//	write left
			n->message= Message( TO_LEFT, fromParent.getk() );
		}
		else if ( CH_NUM( value )==2 && fromRight.zero() )
		{								//	begin writing right
			n->message= Message( TO_RIGHT, fromParent.getk() );
            n->step= 1;
		}
		else
		{
            if ( value==VK_APPLY ){
			    n->message= Message( TO_PARENT, VK_APPLY );
			    n->state  = VMS_BUSY;
            }
            else{
                assert( fromParent.getk()==VK_EOF || fromParent.getk()==VK_EMPTY );
				n->message= Message( TO_PARENT, fromParent.getk() );  
				n->state  = VMS_READY;
			}
            n->step= 0;
		}
	}
    break;
		
	case VMS_BUSY:	
        //  Processing.
        //  Value must be application
        //  Wait for children to be ready, then do left's function
	{
		if ( (!fromLeft.zero() && !fromRight.zero()) || step==1 ){
			switch ( fromLeft.getk() ){
                case VK_S0:
                case VK_K0:
				case VK_I0:							//	I main
				{								      //	add argument
					n->value= VKind(fromLeft.getk() +1); //	K0 -> K1, S0 -> S1, I0 -> TRANSIT
					n->state= VMS_READY;		          
    			    n->message= Message( TO_RIGHT, CMD_BOMB );
                    n->swapRL= !swapRL;     //  transit to right child
				}
                break;
				case VK_S1:
				{								      //	add argument
					n->value= VKind(fromLeft.getk() +1); //	 S1 -> S2
					n->state= VMS_WAIT;		          //	WRITE
				    n->step= 1;
    			    n->message= Message( TO_LEFT, CMD_FINALIZE );
				}
                break;
				case VK_K1:							//	K main
				{
                    if ( step==0 ){
					    n->step= 1;
    			        n->message= Message( TO_RIGHT, CMD_BOMB );
                    }
                    else{
					    //n->step= 0;       must remain "1"
					    n->value= VK_TRANSIT;
					    n->state= VMS_WAIT;
    			        n->message= Message( TO_LEFT, CMD_FINALIZE );
                    }
				}
                break;
				case VK_S2:							//	S main
				{
                    if ( fromRight.getk()==VK_APPLY )
                        break;
                    if ( step==0 ){
					    n->step= 1;
			            n->message= Message( TO_LEFT, CMD_APPLY );
                    }
                    else{
					    n->step= 0;
			            n->state= VMS_PUMP;
	                    n->message= Message( TO_RIGHT, CMD_COPY );
                    }
				}
                break;
            }
        }
	}
    break;
		
	case VMS_WAIT:	
    if ( step==1 ){
        n->message= Message( TO_PARENT, VK_EMPTY );
	    n->step= 0;
    }
    if ( fromLeft.getk() != VK_EMPTY ){
        n->state= VMS_READY;
    }
    break;
		
	case VMS_PUMP:	
        //  Convert right child to string, then send
        //  it to the left for application
        switch ( step ){
        case 0:
	        n->message= Message( TO_LEFT, fromRight.getk() );
            if ( fromRight.getk() == VK_EOF ){
		        n->step= 1;
            }
        break;
        case 1:
 	        n->step= 0;
            n->message= Message( TO_RIGHT, CMD_BOMB );
	        n->value= VK_TRANSIT;
	        n->state= VMS_READY;
        break;
        }
    break;
		
	case VMS_COPY:	
        //  Read self and children, send to parent as string
	{
        if ( message.is_eof() )
		{
			n->message= Message( TO_PARENT, value );
			n->state= VMS_READY;
			n->step= 0;
		}
		else if ( step==0 && fromLeft.getk() != VK_EOF )
		{
			n->message= Message( TO_PARENT, fromLeft.getk() );
		}
		else if ( CH_NUM( value )==1 )
		{
			n->message= Message( TO_PARENT, VK_EOF );
		}
		else if ( step==0 )
		{
			n->message= Message( TO_LEFT, CMD_ECHO );   //  make delay
			n->step= 1;
		}
		else if ( step==1 )
		{
            if ( fromLeft.getk()==VK_EOF ){     //  echo returned
			    n->message= Message( TO_RIGHT, CMD_COPY );
			    n->step= 2;
            }
            else
    			n->message= Message( TO_PARENT, CMD_EMPTY );
		}
		else if ( fromRight.getk() != VK_EOF )
		{
			n->message= Message( TO_PARENT, fromRight.getk() );
		}
		else 
		{
			n->message= Message( TO_PARENT, VK_EOF );
		}
	}
    break;
		
	case VMS_READY:	
        //  Calculated, waiting commands from parent
	{
		switch ( fromParent.getm() ){
            //  Command to read self
		    case CMD_COPY:						//	read self
		    {
			    if ( CH_NUM( value )==0 )
			    {									//	begin 1
			        n->message= Message( TO_PARENT, VK_EOF );
			    }
			    else 
			    {									//	begin 2
			        n->message= Message( TO_LEFT, CMD_COPY );
			    }
			    if ( value != VK_TRANSIT || step==1 )
			    {
				    n->state= VMS_COPY;
				    n->step = 0;
			    }
			    else
				    n->step= 1;
		    }
            break;
            //  Command to apply string to self
		    case CMD_APPLY:						//	apply string from parent to itself
		    {
				switch ( value ){
                    case VK_K0:
                    case VK_S0:
				    case VK_S1:
				    case VK_I0:						      //	I main
				    {								      //	add argument
					    n->value= VKind(value +1);	      //	K0 -> K1, I0 -> TRANSIT, S0 -> S1, S1 -> S2
					    n->state= VMS_EMPTY;		      //    WRITE
    			        n->message= Message( TO_PARENT, VK_EMPTY );
				    }
                    break;
				    case VK_K1:							//	K main
				    {
					    n->value= VK_TRANSIT;
					    n->state= VMS_IGNORE;
    			        n->message= Message( TO_PARENT, VK_EMPTY );
				    }
                    break;
				    case VK_S2:							//	S main
				    {
					    n->state= VMS_APPLY;
        			    n->message= Message( TO_CHILDREN, CMD_APPLY );
				    }
                    break;
				    case VK_TRANSIT:
				    {
					    n->state= VMS_EMPTY;
        			    n->message= Message( TO_LEFT, CMD_APPLY );
				    }
                }
		    }
            break;
            //  Search for depth of the last letter from subtree
		    case CMD_ECHO:
            {
                switch( CH_NUM( value ) )
                {
                case 0:
			        n->message= Message( TO_PARENT, VK_EOF );
                    break;
                case 1:
    			    n->message= Message( TO_LEFT, CMD_ECHO );
                    break;
                case 2:
    			    n->message= Message( TO_RIGHT, CMD_ECHO );
                    break;
                }
            }
            break;
            //  Command finalize for K(x), S(x) 
		    case CMD_FINALIZE:
            {
		        if ( value == VK_TRANSIT )
		        {
    			    n->message= Message( TO_LEFT, fromParent.getm() );
                    n->step= 1;         //  to prevent doubling child copy
		        }
		        else{
                    assert( value==VK_K1 || value==VK_S1 );
				    n->value= VK_TRANSIT;
			        n->message= Message( TO_PARENT, fromLeft );
                }
            }
            break;
            //  If there is no command, report value of self
		    default: {
                assert( fromParent.zero() || fromParent.getk()== VK_EOF );
		        if ( value == VK_TRANSIT )
		        {
                    if ( step ){
                        n->step= 0; 
    			        n->message= Message( TO_PARENT, VK_EMPTY );
                    }
                    else
    			        n->message= Message( TO_PARENT, fromLeft );
		        }
		        else
		        {
                    if ( fromLeft==VK_EOF || fromRight==VK_EOF )
    			        n->message= Message( TO_PARENT, VK_EOF );
                    else
    			        n->message= Message( TO_PARENT, value );
		        }
            }
        }
	}
		
    break;
	case VMS_IGNORE:	
        //  ignore string from parent
	if ( fromParent.getk() == VK_EOF )
	{
	    n->state= VMS_READY;
	}
		
    break;
	case VMS_APPLY:	
        //  S(x,y) gets last argument
    if ( step==0 )
	{//	Sxyz -> `(_`_xz) (_`_yz)	
        n->message= Message( TO_CHILDREN, fromParent.getk() );
	}
    else{
        //  finalize
		n->state= VMS_BUSY;
        n->message= Message( TO_PARENT, VK_EMPTY );
        n->step= 0;
    }
	if ( fromParent.getk() == VK_EOF )
	{	//	Sxyz -> _`_ (`xz) (`yz)
		n->value= VK_APPLY;	
        n->step= 1;
	}
    }
}




