
#include<map>

#include "lang_builtin.h"

//struct IsApplied
//{
//	Functor::DebugInfo func;
//	Functor::DebugInfo data;
//	int slot;
//	IsApplied( Functor::DebugInfo f, Functor::DebugInfo d, int s )
//		:func(f), data(d), slot(s)	{}
//};

template< class T >
int	IsApplied( typename Functor<T>::DebugInfo f, typename Functor<T>::DebugInfo d, int s )
{
	return (int(f)<<20)
			+(int(d))
			+(int(s)<<12);
}

extern std::map< int, int> applyCount;

template< class T >
void incrApllies( Functor<T>* f, Functor<T>* arg, int slot=0 );



