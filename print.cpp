
#include "stdafx.h"
#include "lang_builtin.h"
#include "print.h"

using namespace std;

extern myStream* stru;
myStream* aply;


template void CombI<struct FunctorType1>::print(void) const;
template void Prn<struct FunctorType1>::print(void) const;
template void Enter<struct FunctorType1>::print(void) const;
template void CombK<struct FunctorType1>::print(void) const;
template void CombV<struct FunctorType1>::print(void) const;
template void CombS<struct FunctorType1>::print(void) const;
template void CombM<struct FunctorType1>::print(void) const;
template void formDelay<struct FunctorType1>::print(void) const;

template void CombI<struct FunctorType1>::printItem(void) const;
template void Prn<struct FunctorType1>::printItem(void) const;
template void Enter<struct FunctorType1>::printItem(void) const;
template void CombV<struct FunctorType1>::printItem(void) const;
template void CombM<struct FunctorType1>::printItem(void) const;
template void Curried<struct FunctorType1>::printItem(void) const;
template void formDelay<struct FunctorType1>::printItem(void) const;

template void Functor<struct FunctorType1>::preApply( Functor<struct FunctorType1>* arg ) const;



template void CombI<struct FTypeNoRefs>::print(void) const;
template void Prn<struct FTypeNoRefs>::print(void) const;
template void Enter<struct FTypeNoRefs>::print(void) const;
template void CombK<struct FTypeNoRefs>::print(void) const;
template void CombV<struct FTypeNoRefs>::print(void) const;
template void CombS<struct FTypeNoRefs>::print(void) const;
template void CombM<struct FTypeNoRefs>::print(void) const;
template void formDelay<struct FTypeNoRefs>::print(void) const;

template void CombI<struct FTypeNoRefs>::printItem(void) const;
template void Prn<struct FTypeNoRefs>::printItem(void) const;
template void Enter<struct FTypeNoRefs>::printItem(void) const;
template void CombV<struct FTypeNoRefs>::printItem(void) const;
template void CombM<struct FTypeNoRefs>::printItem(void) const;
template void Curried<struct FTypeNoRefs>::printItem(void) const;
template void formDelay<struct FTypeNoRefs>::printItem(void) const;

template void Functor<struct FTypeNoRefs>::preApply( Functor<struct FTypeNoRefs>* arg ) const;


template< class T > void 
CombI<T>::print(void) const
{
    stru->put( 'i', getSrcCharNum() );
}

template< class T > void 
CombI<T>::printItem(void) const
{
    (*aply)<<'i';
}

template< class T > void 
CombM<T>::printItem(void) const
{
    (*aply)<<'m';
}

template< class T > void 
Prn<T>::print(void) const
{
    stru->put( '.', getSrcCharNum() );
    *stru << arg;
}

template< class T > void 
Prn<T>::printItem(void) const
{
    (*aply)<<'.'<<arg;
}

template< class T > void 
Enter<T>::print(void) const
{
    stru->put( 'r', getSrcCharNum() );
}

template< class T > void 
Enter<T>::printItem(void) const
{
    (*aply)<<'r';
}

template< class T > void 
CombK<T>::print(void) const
{
    stru->put( 'k', getSrcCharNum() );
    if ( args.size()!=0 ){
        *stru << '[';
        args[0]->print();
        *stru << ']';
    }
}

template< class T > void 
CombV<T>::print(void) const
{
    *stru << 'v';
}

template< class T > void 
CombV<T>::printItem(void) const
{
    (*aply)<<'v';
}

template< class T > void 
CombM<T>::print(void) const
{
    stru->put( 'm', getSrcCharNum() );
}

template< class T > void 
formDelay<T>::print(void) const
{
    *stru << 'd';
}

template< class T > void 
formDelay<T>::printItem(void) const
{
    (*aply)<<'d';
}

template< class T > void 
CombS<T>::print(void) const
{
    switch (stage){
    case 0:
        stru->put( 's', getSrcCharNum() );
        if ( args.size()!=0 ){
            *stru << '[';
            if ( args.size()>0 ){
                args[0]->print();
            }
            if ( args.size()==2 ){
                *stru << ',';
                args[1]->print();
            }
            *stru << ']';
        }
        break;
    case 1:
        args[0]->print();
        *stru << '(';
        args[1]->print();
        *stru << '(';
        tmpZ->print();
        *stru << ')';
        *stru << ')';
        break;
    case 2:
        args[0]->print();
        *stru << '(';
        args[1]->print();
        *stru << ')';
        break;
    case 3:
        args[0]->print();
        break;
    }
    return;
}

template< class T > void 
Curried<T>::printItem(void) const
{
    (*aply) << strHead();
	if ( args.size()>0 ){
        *aply << '[';
        args[0]->printItem();
		if ( args.size()==2 ){
			*aply << ',';
			args[1]->printItem();
		}
        *aply << ']';
	}
}

template< class T > void 
Functor<T>::preApply( Functor<T>* arg ) const
{
	printItem();
	*aply << '\n';
	arg->printItem();
	*aply << '\n' << '\n';
}


void prepareApplyStats(){
	aply= new myStream( *(new ofstream( "apply_report.txt" )) );
}

