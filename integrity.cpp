
#include "stdafx.h"
#include "print.h"
#include "lang_builtin.h"

using namespace std;

template< class T >
Functor<T>::Functor(  const DebugInfo &_di  )
:di(_di)
{
    if ( integrity1 ){
        static int constCalls=0;
        cerr << "  0x" << hex << (void*)this << " " << dec << ++constCalls << endl;
    }
}

//extern Functor<FunctorType1>* top;
int nodesCount=0;

template< class T >
void countNodes( Functor<T>* f )
{
    nodesCount++;
    cerr << " " << f->getSrcCharNum();
    return;
}

template< class T >
int calcNodeCount()
{
    nodesCount=0;
    Functor<T>::top->visitAll( countNodes );
    return nodesCount;
}

template< class T >
Functor<T>::~Functor()
{
    if ( integrity1 ){
        static int destrCalls=0;
        cerr << " ~0x" << hex << (void*)this 
                << " destructor calls: " << dec << ++destrCalls
//                << " calcNodeCount: " << calcNodeCount() 
                << endl;
    }
}

template< class T >
void dumpNodeCount()
{
    if ( Functor<T>::integrity1 ){
        static int destrCalls=0;
        cerr << " calcNodeCount: " << dec << calcNodeCount<T>() << endl;
    }
}

extern volatile int workMode;

template< class T >
CombS<T>::CombS( const DebugInfo &_di )
:stage(0),Curried(_di)
{
    static int counter=0;
    if ( counter++ == 1000 ){
    }
    if ( workMode ){
        workMode =2;
		while(true);
    }
}

extern string prog;
//Functor::VisitorType printRelative;
ofstream* rela;

myStream* stru;

template< class T >
void printRelative( Functor<T>* node )
{
    int i= node->getSrcCharNum();
    while (i--)
        *rela << ' ';
    *rela << '*' << endl;
}

template< class T >
void showRelativity( Functor<T>* top )
{
    static int inst= 0;
    char fn[32];
    sprintf( fn, "relativity%d.dump", inst );
    rela= new ofstream( fn );
    *rela << prog << endl;
    top->visitAll( printRelative );
    delete rela;
    inst++;
}

template< class T >
void showStructure( Functor<T>* top )
{
    static int inst= 0;
    char fn[32];
    sprintf_s( fn, "structure%d.dump", inst );
    stru= new myStream( *(new ofstream( fn )) );
    stru->levelDown() << prog << endl;
    top->print();
    stru->levelDown() << endl;
    delete stru;
    inst++;
}


template CombS<struct FunctorType1>::CombS( const DebugInfo &_di );
template Functor<struct FunctorType1>::Functor( const DebugInfo &_di  );
template CombS<struct FTypeNoRefs>::CombS( const DebugInfo &_di );
template Functor<struct FTypeNoRefs>::Functor( const DebugInfo &_di  );


template void countNodes( Functor<struct FunctorType1>* f );
template void printRelative( Functor<struct FunctorType1>* f );
template void showStructure( Functor<struct FunctorType1>* f );
template void dumpNodeCount<struct FunctorType1>();

template void countNodes( Functor<struct FTypeNoRefs>* f );
template void printRelative( Functor<struct FTypeNoRefs>* f );
template void showStructure( Functor<struct FTypeNoRefs>* f );
template void dumpNodeCount<struct FTypeNoRefs>();



