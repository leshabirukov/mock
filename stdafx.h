// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#ifndef _WIN32_WINNT		// Allow use of features specific to Windows XP or later.                   
#define _WIN32_WINNT 0x0501	// Change this to the appropriate value to target other versions of Windows.
#endif						

#include <cstdio>
#include <cstring>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <deque>
#include <cassert>

#ifdef _WIN32
#include <conio.h>
#define getch _getch
#else
#include <termios.h>
#include <sys/ioctl.h>

struct TerminalOpt {
TerminalOpt() {
termios new_settings;
tcgetattr(0,&stored_settings);
new_settings = stored_settings;
new_settings.c_lflag &= (~ICANON);
new_settings.c_cc[VTIME] = 0;
new_settings.c_cc[VMIN] = 1;
tcsetattr(0,TCSANOW,&new_settings);
}
~TerminalOpt() {
tcsetattr(0,TCSANOW,&stored_settings);
}
termios stored_settings;
};

#define getch getchar

#endif
// TODO: reference additional headers your program requires here
