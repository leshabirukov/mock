

all : mockingbird

mockingbird : envi_graph.o unlambda.o \
		envi_print.o utility.o graph_step.o
	$(CXX) $(LDFLAGS) -o $@ $^

unlambda.o: unlambda.cpp stdafx.h envi_graph.h
	$(CXX) -c $(CFLAGS) $<

envi_graph.o: envi_graph.cpp stdafx.h envi_graph.h lang_builtin.h
	$(CXX) -c $(CFLAGS) $<

envi_print.o: envi_print.cpp stdafx.h envi_graph.h lang_builtin.h
	$(CXX) -c $(CFLAGS) $<

graph_step.o: graph_step.cpp stdafx.h envi_graph.h
	$(CXX) -c $(CFLAGS) $<

utility.o: utility.cpp stdafx.h
	$(CXX) -c $(CFLAGS) $<

cleanall : clean
	-rm mockingbird *.log

clean :
	-rm *.o mockingbird

 