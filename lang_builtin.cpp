
#include "stdafx.h"
#include "lang_builtin.h"
#include "statistics.h"
#include <ostream>

using namespace std;

typedef Functor<struct FunctorType1> Functor1;
typedef Functor<struct FTypeNoRefs> FunctorNoRefs;

template Functor1* CombV<struct FunctorType1>::apply(Functor1 *f); 
template Functor1* formDelay<struct FunctorType1>::apply(Functor1 *f);
template Functor1* CombI<struct FunctorType1>::apply(Functor1 *f);
template Functor1* CombM<struct FunctorType1>::apply(Functor1 *f);
template Functor1* Prn<struct FunctorType1>::apply(Functor1 *f);
template Functor1* Enter<struct FunctorType1>::apply(Functor1 *f);
template Functor1* Curried<struct FunctorType1>::copy() const;
template Functor1* Curried<struct FunctorType1>::apply( Functor1* f );
template Functor1* CombK<struct FunctorType1>::applyLast( Functor1* f );
template Functor1* CombS<struct FunctorType1>::applyLast( Functor1* f );

template FunctorNoRefs* CombV<struct FTypeNoRefs>::apply(FunctorNoRefs *f); 
template FunctorNoRefs* formDelay<struct FTypeNoRefs>::apply(FunctorNoRefs *f);
template FunctorNoRefs* CombI<struct FTypeNoRefs>::apply(FunctorNoRefs *f);
template FunctorNoRefs* CombM<struct FTypeNoRefs>::apply(FunctorNoRefs *f);
template FunctorNoRefs* Prn<struct FTypeNoRefs>::apply(FunctorNoRefs *f);
template FunctorNoRefs* Enter<struct FTypeNoRefs>::apply(FunctorNoRefs *f);
template FunctorNoRefs* Curried<struct FTypeNoRefs>::copy() const;
template FunctorNoRefs* Curried<struct FTypeNoRefs>::apply( FunctorNoRefs* f );
template FunctorNoRefs* CombK<struct FTypeNoRefs>::applyLast( FunctorNoRefs* f );
template FunctorNoRefs* CombS<struct FTypeNoRefs>::applyLast( FunctorNoRefs* f );

//#define UNL_IMPL_NO_REFS
//#ifdef UNL_IMPL_NO_REFS

FunctorNoRefs*
CombV<FTypeNoRefs>::apply(FunctorNoRefs *f) 
{
    delete f;
    return this;
}

FunctorNoRefs*
formDelay<FTypeNoRefs>::apply(FunctorNoRefs *f) 
{
    delete f;
    return this;
}

FunctorNoRefs*
CombI<FTypeNoRefs>::apply(FunctorNoRefs *f) 
{
    delete this;
    return f;
}

FunctorNoRefs*
CombM<FTypeNoRefs>::apply(FunctorNoRefs *f) 
{
    delete this;
    return f->apply( f );
}

template< class T > void dumpNodeCount();

int ent=0;
FunctorNoRefs* FunctorNoRefs::top;
Functor1* Functor1::top;

FunctorNoRefs*
Prn<FTypeNoRefs>::apply(FunctorNoRefs *f)
{
    if (ent==6)
        showStructure( top );
    cout << arg;
    dumpNodeCount<FTypeNoRefs>();
    delete this;
    return f;
}


FunctorNoRefs*
Enter<FTypeNoRefs>::apply(FunctorNoRefs *f)
{
    ent++;
    cout << endl;
    delete this;
    return f;
}

FunctorNoRefs*
Curried<FTypeNoRefs>::copy() const
{
    Curried* res= copyHead();
    size_t i;
    for ( i=0; i<args.size(); i++ ){
        res->args.push_back( args[i]->copy() );
    }
    return res;
}

FunctorNoRefs*
Curried<FTypeNoRefs>::apply( FunctorNoRefs* f )
{
    if ( args.size()==aNum() ){
        Functor* r= applyLast( f );
        delete this;
        return r;
    }
    args.push_back( f );
    return this;
}

FunctorNoRefs* 
CombK<FTypeNoRefs>::applyLast( FunctorNoRefs* f )
{
    delete f;
    Functor* r= args[0];
    return r;
}

//extern FunctorNoRefs* top;

FunctorNoRefs* 
CombS<FTypeNoRefs>::applyLast( FunctorNoRefs* f )
{
    Functor* fcopy= f->copy();
    tmpZ= fcopy;
    stage =1;
    Functor* first= args[0]->apply( f );
    args[0]=first;
    stage =2;
    Functor* second= args[1]->apply( fcopy );
    stage =3;
    args[1]=second;
    if ( top==this ){
        top= first;
//        cerr << " now top is 0x" << top << dec << endl;
    }
    return first->apply( second );
}

//#else //UNL_IMPL_NO_REFS

template< class T > Functor<T>*
CombV<T>::apply(Functor<T> *f) 
{
	preApply( f );
	incrApllies( this, f );
    return new CombV(di);
}

template< class T > Functor<T>*
formDelay<T>::apply(Functor<T> *f) 
{
	preApply( f );
	incrApllies( this, f );
    return new CombV<T>(di);
}

template< class T > Functor<T>*
CombI<T>::apply(Functor<T> *f) 
{
	preApply( f );
	incrApllies( this, f );
    return f;
}

template< class T > Functor<T>*
CombM<T>::apply(Functor<T> *f) 
{
    return f->apply( f );
}

extern Functor<FunctorType1>* top;

template< class T > Functor<T>*
Prn<T>::apply(Functor<T> *f)
{
	preApply( f );
	incrApllies( this, f );
    cout << arg;
    return f;
}


template< class T > Functor<T>*
Enter<T>::apply(Functor<T> *f)
{
	preApply( f );
	incrApllies( this, f );
    static int i=0;
    if (i++<10)
        showStructure<T>( top );
    cout << endl;
    return f;
}

template< class T > Functor<T>*
Curried<T>::copy() const
{
    Curried* res= copyHead();
    size_t i;
    for ( i=0; i<args.size(); i++ ){
        res->args.push_back( args[i] );
    }
    //std::copy( args.begin(), args.end(), res->args.begin() );->copy()
    return res;
}

template< class T > Functor<T>*
Curried<T>::apply( Functor<T>* f )
{
    //static int i=0;
    //static int j=10;
    //if ( i++>j ){
    //    showRelativity( f );
    //    showRelativity( this );
    //    j= j*1.1;
    //}
	preApply( f );
	incrApllies( this, f, (int)args.size() );

    if ( args.size()==aNum() )
        return applyLast( f );
    Curried* newOne= dynamic_cast<Curried*>( copy() );
    newOne->args.push_back( f );
    return newOne;
}

template< class T > Functor<T>* 
CombK<T>::applyLast( Functor<T>* f )
{
    return args[0];
}

template< class T > Functor<T>* 
CombS<T>::applyLast( Functor<T>* f )
{
    stage =1;
    tmpZ= f;
    Functor<T>* first= args[0]->apply( f );
    stage =2;
    Functor<T>* second= args[1]->apply( f );
    stage =3;
    return first->apply( second );
}
//#endif //UNL_IMPL_NO_REFS
