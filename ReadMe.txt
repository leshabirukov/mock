========================================================================
    CONSOLE APPLICATION : unlambda Project Overview
========================================================================

AppWizard has created this unlambda application for you.  

This file contains a summary of what you will find in each of the files that
make up your unlambda application.


unlambda.vcproj
    This is the main project file for VC++ projects generated using an Application Wizard. 
    It contains information about the version of Visual C++ that generated the file, and 
    information about the platforms, configurations, and project features selected with the
    Application Wizard.

unlambda.cpp
    This is the main application source file.

/////////////////////////////////////////////////////////////////////////////
Other standard files:

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named unlambda.pch and a precompiled types file named StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////
Other notes:

AppWizard uses "TODO:" comments to indicate parts of the source code you
should add to or customize.

/////////////////////////////////////////////////////////////////////////////


void
GState::step()
{
	std::map< Position, VState > newStateMap= stateMap;
	int i, j;
	for ( j=0; j<master.y_sz; j++){
		for ( i=0; i<master.y_sz; i++){
			Position p= Position(j,i);
			VState& vs= getState( p );
            VState& newVs= newStateMap[ p ];
            if ( vs.state == VMS_READ ){
                int chn;
                //  ���� ����������������� �������
                for ( chn=0; chn<vs.chNum; chn++ ){
    			    VState& vch1= getChild( newVs, chn );
                    if ( vch1.state != VMS_DONE )
                        break;
                }
                //  �� ������� - �������� � ����, ���� � ���
                if ( chn==vs.chNum ){
                    newVs.state = VMS_DONE;
                    for ( chn=0; chn<vs.chNum; chn++ ){
                        VState& vch1= getChild( newVs, chn );
                        vch1.state =VMS_READY;
                    }
                    continue;
                }
                VState& vch1= getChild( newVs, chn );
                if ( vch1.state == VMS_READ ){
                    //  ������� ����� ������������
                    continue;
                }
                else if ( vch1.state == VMS_READY ){
                    if ( chn==0 )
                        cout << "-" << vs.mnemonic( vs.kind );
                    vch1.state =VMS_READ;
                    continue;
                }
                else{
                    assert(0);                
                }
            }

            if ( vs.kind==VK_BOMB ){
                newVs.kind= VK_EMPTY;
                int i;
                for ( i=0; i<vs.chNum; i++ )
                    newStateMap[ *newVs.child[i] ].kind= VK_BOMB;
                newVs.chNum= 0;
            }
            else if ( vs.kind==VK_KX ){
                newVs.kind= VK_TRANSIT;
                //  delete k from 'kxy
                newStateMap[ *newVs.child[0] ].kind= VK_BOMB;
                newVs.child[0]=newVs.child[1];
                newVs.chNum= 1;
            }
            if ( vs.kind!=VK_APPLY )
                continue;
			VState vch1= getChild( newVs, 0 );

            if ( !( vch1.state== VMS_READY && 
                    getState(*vs.child[1]).state== VMS_READY ))
                continue;
            if ( vch1.curry>1 ){
                newVs.kind= vch1.kind;
                newVs.curry=vch1.curry-1;
                newVs.state= VMS_READY;
            }
            else if ( vch1.kind==VK_K ){
                vch1.kind= VK_KX;
                //  delete y from 'kxy
                newStateMap[ *newVs.child[1] ].kind= VK_BOMB;
                newVs.kind= VK_TRANSIT;
                newVs.chNum= 1;
            }
            else if ( vch1.kind==VK_M ){
                newStateMap[ *newVs.child[1] ].state= VMS_READ;
                //  delete y from 'kxy
            }
		}
	}
    stateMap= newStateMap;
}

