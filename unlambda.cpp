// unlambda.cpp : Defines the entry point for the console application.
//

#define _CRT_SECURE_NO_WARNINGS

#include "stdafx.h"
#include <stdlib.h>
#include "envi_graph.h"

using namespace std;

extern GState gState;

namespace options
{
    bool showNet= false;
    bool interact= false;
}

void printUsage();

int main(int argc, char* argv[])
{
#ifndef _WIN32
    TerminalOpt term;    // for linux getch
#endif
    
    vector<string> params;
    string prog_str= "``kk``s``ss`ss``s`ss``ss`ss";
    int ii;
    for ( ii=1; ii<argc; ii++ ){
        params.push_back( argv[ii] );
    }
    size_t i;
    try{
        for ( i=0; i<params.size(); i++ ){
            cout << params[i] << endl;
            if ( params[i]== "--show-net" )
                options::showNet= true;
            else if ( params[i]== "--interact" )
                options::interact= true;
            else if ( params[i].substr( 0, 10 ) == "--program=" ){
                prog_str= params[i].substr( 10 );
            }
            else if ( params[i][0]== '@' ){
                FILE* commands= fopen( params[i].c_str() +1, "r" ); 
                if ( commands ==NULL ){
                    throw string( "can't open file " ) 
                                    + string( params[i].c_str() +1);
                }

                char _buff[256];
                while ( fgets( _buff, 255, commands ) ){
                    char* buff= _buff;
                    char* next_token =NULL; 
                    while( true ){
                        next_token= strtok( buff, " \n\r" );
                        buff= NULL;
                        if ( next_token ==NULL )
                            break;
                        params.push_back( next_token );
                    }
                }
            }
            else {
                printUsage();
                return 1;
            }
        }
//      b = ``s`ksk   m = ``sii
//		gState.placeFromString( "``````sii````s`kskk``siikkk" );
//		gState.placeFromString( "```si``skk``si``skk" );
//		gState.placeFromString( "``ss``kk``skk" );
//		gState.placeFromString( "```ss``kks`ss" );
        gState.placeFromString( prog_str.c_str() );
//		gState.placeFromString( "```s``skk``skk``s``skk``skk" );
        for (i=0; i<300; i++ ){
            cout << endl << endl << "=== day " << i << " ===";
            //int j=i;
            //int k, m;
            //for ( m=0; m<10; m++ ){
            //    cout << endl;
            //    for ( k=0; k<100; k++ )
            //        cout << j;
            //}
	        //gState.printTree( cout, true );
	        //gState.print( cout );
            gState.step();
            if ( !gState.toOutside.zero() && gState.toOutside.getk() != VK_APPLY )
                break;
        }
	}
	catch( UserStopEsc ){
		cout << "bye!" << endl;
	}
	catch( char* msg ){
		cout << msg << endl;
        return 1;
	}
	catch( string msg ){
		cout << msg << endl;
        return 1;
	}
	cout << endl;
    return 0;
}

