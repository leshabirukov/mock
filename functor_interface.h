

template < class T > class Functor
{
public:
    typedef int DebugInfo;//    ����� �������
protected:
    DebugInfo di;
public:
    static const bool integrity1=false;
    Functor( const DebugInfo &_di );
    ~Functor();
    virtual Functor* copy() const =0;
    virtual Functor* apply( Functor* ) =0;
    //  �������� ��� ���� ������ �������� (����)�������
    typedef void (*VisitorType)(Functor*);
    virtual void visitAll( VisitorType v ){   v(this);  }
    int getSrcCharNum() const { return di; }
	//	���������� ����������� ������
    virtual void print(void) const =0;
	//	���������� �������
    virtual void printItem(void) const =0;
	void preApply( Functor* ) const;

	static Functor* top;
};

template < class T > class CombI: public Functor<T>
{
public:
    CombI( const DebugInfo &_di ):Functor(_di){}
    virtual Functor* copy() const { return new CombI( di ); }
    virtual Functor* apply( Functor* f );
    void print(void) const;
    virtual void printItem(void) const;
};

template < class T > class Prn: public Functor<T>
{
    char arg;
public:
    Prn( char c, const DebugInfo &_di ):arg(c), Functor(_di){}
    virtual Functor* copy() const { return new Prn( arg, di ); }
    virtual Functor* apply( Functor* f );
    void print(void) const;
    virtual void printItem(void) const;
};

template < class T > class Enter: public Prn<T>
{
public:
    Enter( const DebugInfo &_di ):Prn('\n',_di){}
    virtual Functor* copy() const { return new Enter( di ); }
    virtual Functor* apply( Functor* f );
    void print(void) const;
    virtual void printItem(void) const;
};

template < class T > class Curried: public Functor<T>
{
    virtual int aNum() const =0;
    virtual Functor* applyLast( Functor* f ) =0;
    virtual Curried* copyHead() const =0;
protected:
    std::vector<Functor*> args;
public:
    Curried( const DebugInfo &_di ):Functor(_di){}
    virtual Functor* copy() const;
    virtual Functor* apply( Functor* f );
    virtual void visitAll( VisitorType );
    virtual char strHead(void) const =0;
    virtual void printItem(void) const;
};

template < class T > class CombK: public Curried<T>
{
    virtual int aNum() const {  return 1;   }
    virtual Functor* applyLast( Functor* f );
    virtual Curried* copyHead() const {    return new CombK( di );   }
public:
    CombK( const DebugInfo &_di ):Curried(_di){}
    void print(void) const;
	char strHead(void) const {	return 'k';}
};

template < class T > class CombS: public Curried<T>
{
    typedef int Stage;
    Stage stage;
    Functor* tmpZ;
    virtual int aNum() const {  return 2;   }
    virtual Functor* applyLast( Functor* f );
    virtual Curried* copyHead() const {    return new CombS( di );   }
public:
    CombS<T>( const DebugInfo &_di );

    void print(void) const;
	char strHead(void) const {	return 's';}
};

template < class T > class CombV: public Functor<T>
{
    virtual Functor* copy() const { return new CombV( di ); }
    virtual Functor* apply( Functor* f );
public:
    CombV( const DebugInfo &_di ):Functor(_di){}
    void print(void) const;
    virtual void printItem(void) const;
};

template < class T > class CombM: public Functor<T>
{
    virtual Functor* copy() const { return new CombM( di ); }
    virtual Functor* apply( Functor* f );
public:
    CombM( const DebugInfo &_di ):Functor(_di){}
    void print(void) const;
    virtual void printItem(void) const;
};

template < class T > class formDelay: public Functor<T>
{
    virtual Functor* copy() const { return new formDelay( di ); }
    virtual Functor* apply( Functor* f );
public:
    formDelay( const DebugInfo &_di ):Functor(_di){}
    void print(void) const;
    virtual void printItem(void) const;
};

template < class T > void showRelativity( Functor<T>* top );
template < class T > void showStructure( Functor<T>* top );


