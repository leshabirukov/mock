
#include "stdafx.h"
#include <stdlib.h>
#include <windows.h>
#include "lang_builtin.h"

using namespace std;


typedef FTypeNoRefs FType;
//typedef FunctorType1 FType;

string prog;

template void showRelativity<FType>( Functor<FType>* top );
template void showStructure<FType>( Functor<FType>* top );

int processCallNo =0;
int volatile workMode;
Functor<FType>::DebugInfo dInfo= -1;

void prepareApplyStats();

Functor<FType>* process()
{
	prepareApplyStats();

	int callNo= processCallNo++;
    static bool d_mode( false );
    char c;
    Functor<FType>* f= 0;
    Functor<FType>** curr= &f;
    while( dInfo++, c= cin.get() ){
		if ( cin.eof() )
			break;
        prog.push_back(c);
        Functor<FType>* g= 0;
        if ( c!='`' )
            cerr << c;
        switch (c)
        {
        case' ':
        case'\n':
            continue;
        case '`': 
            *curr= process();  
            break;
        case '.':
            char lch;
            lch= cin.get();
            if ( cin.bad() )
                throw "cin input error";
            *curr= new Prn<FType>( lch, dInfo );
            dInfo++;
            prog.push_back(lch);
            cerr << lch;
            break;
        case 'k':   *curr= new CombK<FType>( dInfo ); break;
        case 's':   *curr= new CombS<FType>( dInfo ); break;
        case 'i':   *curr= new CombI<FType>( dInfo ); break;
        case 'v':   *curr= new CombV<FType>( dInfo ); break;
        case 'm':   *curr= new CombM<FType>( dInfo ); break;
        case 'r':   *curr= new Enter<FType>( dInfo ); break;
        case 'd':   *curr= new formDelay<FType>( dInfo ); break; 
                   
        default:    throw "unlambda interpreter: unknown comand";
        }
        d_mode= dynamic_cast<formDelay<FType>*>(*curr) !=0; 
        Functor<FType>* Res= 0;
        if ( curr == &f ){
            curr= &g;
            cerr << '(';
        }
        else{
            cerr << ')';
//            cerr << " Now top is 0x" << top << dec << endl;
            Functor<FType>::top= f;
            if ( callNo ==1 )
                showStructure( Functor<FType>::top );
            return f->apply( g );
        }
    }
    d_mode= false;
    return 0;
}

DWORD workingThread (LPVOID lpdwThreadParam )
{
    try{
	    process();
    }
    catch ( char* msg ){
        cout << msg << endl;
    }
    return 0;
}

