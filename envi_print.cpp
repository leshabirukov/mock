
#include "stdafx.h"
#include <algorithm>
#include "envi_graph.h"

using namespace std;

int maxx=5;
int maxy=5;

char 
VState::mnemonic( VKind k )
{
	switch( k ){
		case VK_EMPTY:	return '.';
		case VK_APPLY:	return '>';	
		case VK_S0:	    return 's';
		case VK_S1:	    return 'S';
		case VK_S2:	    return '2';	
		case VK_K0:	    return 'k';	
        case VK_K1:	    return 'K';	
		case VK_I0:	    return 'i';	
        case VK_TRANSIT:return '|';	
        case VK_EOF:    return 'E';	
        default: assert(0);
	}  
    return '\0';
}

char 
VState::mnemonic_ms( VMeta ms )
{
	switch( ms ){
		case VMS_EMPTY:     return '.';
		case VMS_BUSY:      return '!';
		case VMS_READY:	    return '_';	
		case VMS_COPY:	    return '?';	
        case VMS_APPLY:     return '>';
        case VMS_PUMP:      return '^';
        case VMS_IGNORE:    return '-';
        case VMS_WAIT:      return ':';
        default: assert(0);
	}  
    return '\0';
}

char 
VState::mnemonic_cmd( VComm c )
{
	switch( c ){
		case CMD_EMPTY:     return '.';
		case CMD_FINALIZE:  return '$';	
		case CMD_COPY:	    return '?';	
        case CMD_BOMB:      return '-';
        case CMD_APPLY:     return '>';
        case CMD_ECHO:      return '@';
        default: assert(0);
	}  
    return '\0';
}

char 
mnemonic( const MessageBody& msg )
{
    return msg.isk ? VState::mnemonic( msg.k ) : VState::mnemonic_cmd( msg.m );
}

void 
VState::print( ostream& ostr )
{
    if ( value==VK_EMPTY && state==VMS_EMPTY && !message.is_bomb() ){
        ostr << ".   ";
        return;
    }
    ostr << mnemonic( value );
    ostr << mnemonic_ms( state );

    if ( !message.zero() && message.targ )
        setConsoleColor( ( 
            ( (message.targ &1) ? FOREGROUND_GREEN : 0   ) | 
            ( (message.targ &2) ? FOREGROUND_BLUE |FOREGROUND_GREEN : 0   ) | 
                    (message.isk? 0: FOREGROUND_INTENSITY ) ));

    ostr << ::mnemonic( message );
    if ( swapRL ){
        setConsoleColor( FOREGROUND_RED | FOREGROUND_BLUE );
        ostr << '<';
    }
    else
        ostr << ' ';
    setConsoleColor( (FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN));
}

void 
GState::print( ostream& ostr, Position initPos )
{
	Position i, j;
    Position next;
	ostr << endl << endl;
    
    StateMapType::iterator pit= stateMap.begin();
    next= pit->first;
    int tab= 32;
	for ( j=1;   options::interact ? (j<64) : (next!= 0);    j=j<<1){
		for ( i=0; i<j; i++){
			Position p= Position( i +initPos*j );
            if ( p<next ){
                ostr << ".   ";      // node not vizited
            }
            else {
                if ( p!=next ){
                    pit= stateMap.lower_bound( p );
                    if ( pit == stateMap.end() ){
                        next= 0;
                        break;
                    }
                    else
                        next= pit->first;
                }
			    VState& v= getState( p );
                v.print( ostr );
                pit++;
                if ( pit == stateMap.end() )
                    next= 0;
                else
                    next= pit->first;
            

            //    ostr << v.step;
            }
            //ostr << v.step;
            //ostr << mnemonic( v.message.read( TO_RIGHT ));
            //ostr << (v.swapRL ? "s":" ");
            //int k;
            //for( k=0; k<j; k++)
            int t;
            for ( t=1; t<tab; t++ ){
                ostr << "    ";
            }
		}
		ostr << endl;
        tab /=2;
	}
}

void 
GState::printTree( ostream& ostr, bool showTransits )
{
	ostr << endl;
    printTree( ostr, showTransits, getEntry(), false );
}

void 
GState::printTree( ostream& ostr, bool showTransits, Position p, 
                  // ����� ���������� S ����������� ������ ��������, ��� ������ ���� 
                  //    �������� ����� "s", ����� �� �������� ������, ���� ������
                  //    CMD_FINALIZE, ������� ���� ���� ��� �������
                  //    ���� �����, ������ ����������� S �������.
                  bool supressS1 
                  )
{
	VState& v= getState( p );
	char ou;
	switch( v.value ){
		case VK_EMPTY:	ou='.';	break;
		case VK_EOF:	ou='E';	break;
		case VK_APPLY:	ou='`';	break;
		case VK_S0:	    ou='s';	break;	    
		case VK_S1:	    
            if ( supressS1 ){
                if ( !showTransits ){
                    printTree( ostr, showTransits, getChild( p, v.swapRL ? 1:0 ), false );
                    return;
                }
                ou='|';	
            }
            else
                ou='s';	
            break;	    
		case VK_S2:	    
            ou='s';	    
            if ( v.message.m == CMD_FINALIZE )
                supressS1= true;    
            break;
		case VK_K0:	    ou='k';	break;	    
		case VK_K1:	    ou='k';	break;
		case VK_I0:	    ou='i';	break;	    
        case VK_TRANSIT:
            if ( v.message.m == CMD_FINALIZE )
                supressS1= true;    
            if ( !showTransits ){
                printTree( ostr, showTransits, getChild( p, v.swapRL ? 1:0 ), supressS1 );
                return;
            }
            ou='|';	break;
        default: assert(0);
	}
	ostr << ou;
    if ( CH_NUM( v.value ) >0 ){
    	ostr << "( ";
        printTree( ostr, showTransits, getChild( p, v.swapRL ? 1:0 ), supressS1 );
        if ( CH_NUM( v.value )>1 ){
    	    ostr << ", ";
            printTree( ostr, showTransits, getChild( p, v.swapRL ? 0:1 ), false );
        }
    	ostr << " )";
    }
}

