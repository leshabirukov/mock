
#include "stdafx.h"
#include "envi_graph.h"

using namespace std;

//typedef Functor<struct FTypeGraph1> Functor1;

int VState::ccc =1;

GState gState(64);

void 
VState::init( VKind k )
{
    value= k;
    state= VMS_BUSY;
}

const char* 
GState::placeFromString( Position p, const char* prog )
{
	char c=*prog++;
	switch(c){
	case '\0':
		throw "placeFromString: bad program string, incomplete";
	case '`':
		fromOutside= Message( TO_CHILDREN, VK_APPLY);
        step();
    	fromOutside= Message( TO_CHILDREN, VK_EMPTY);
		prog= placeFromString( getChild(p,0), prog );
        step();
		prog= placeFromString( getChild(p,1), prog );
        step();
        step();
		break;
	case 's':
		fromOutside= Message( TO_CHILDREN, VK_S0);
        step();
    	fromOutside= Message( TO_CHILDREN, VK_EMPTY);
		break;
	case 'k':
		fromOutside= Message( TO_CHILDREN, VK_K0);
        step();
    	fromOutside= Message( TO_CHILDREN, VK_EMPTY);
		break;
	case 'i':
		fromOutside= Message( TO_CHILDREN, VK_I0);
        step();
    	fromOutside= Message( TO_CHILDREN, VK_EMPTY);
		break;
	default:
		assert(0);
		break;
	}
	//fromOutside= Message( TO_CHILDREN, VK_EOF);
 //   step();
	//fromOutside= Message( TO_CHILDREN, VK_EMPTY);
	return prog;
}

Position 
GState::getChild( Position p, size_t i )
{
    Position np= Position(int(p)*2+i);
//    assert(np<size);
    return np;
}

Position 
GState::getParent( Position p )
{
    Position np= p/2;
    assert(np>0);
    return np;
}

extern int maxx;
extern int maxy;

void
GState::step( Position p )
{

    Message msg_p= (p==Root) ? fromOutside : getState( getParent( p ) ).message;

    bool imRight= (p!=Root) && getState( getParent( p ) ).swapRL ^ isRight( p );
    MessageBody msg_pb= imRight ? msg_p.read( TO_RIGHT ) : msg_p.read( TO_LEFT ); 

    MessageBody msg_lb= CMD_EMPTY;
    MessageBody msg_rb= CMD_EMPTY;
	VState& vs= getState( p );

    Position pl= getChild( p, !vs.swapRL ? 0 : 1 );
    bool leftBorn=  stateMap.find( pl )!=stateMap.end();
    Position pr= getChild( p, !vs.swapRL ? 1 : 0 );
    bool rightBorn= stateMap.find( pr )!=stateMap.end();
    if ( !isTerminal(p) ){
        if ( leftBorn ){
            Message msg_l= getState( pl ).message;
            msg_lb= msg_l.read( TO_PARENT ); 
        }
        if ( rightBorn ){
            Message msg_r= getState( pr ).message;
            msg_rb= msg_r.read( TO_PARENT ); 
        }
    }
    else
        p=p;
    vs.doStep( msg_pb, msg_lb, msg_rb );

    msg_lb= vs.message.read( TO_LEFT ); 
    msg_rb= vs.message.read( TO_RIGHT ); 

    leftBorn |= !msg_lb.zero();
    rightBorn |= !msg_rb.zero();

    if ( leftBorn ){
        step( pl );
    }
    if ( rightBorn ){
        step( pr );
    }
    if ( vs.n->value==VK_EMPTY && !leftBorn && !rightBorn && p!=Root )   // clean
        stateMap.erase( p );

}

UserStopEsc user_esc;

void
GState::step()
{
    //bool shortage= false;

	//for ( j=1; j<size; j++){
	//	Position p= Position(j);
	//	VState& vs= getState( p );

 //       Message msg_p= (j==1) ? fromOutside : 
 //                       getState( getParent( p ) ).message;

 //       bool swap= (j==1) ? false : getState( getParent( p ) ).swapRL;
 //       MessageBody msg_pb= swap ^ (j&1) 
 //                           ? msg_p.read( TO_RIGHT ) : msg_p.read( TO_LEFT ); 

 //       Message msg_l= (j>=size/2) ? Message( TO_LEFT, CMD_EMPTY) : 
 //                       getState( getChild( p, !vs.swapRL ? 0 : 1 ) ).message;

 //       MessageBody msg_lb= msg_l.read( TO_PARENT ); 

 //       Message msg_r= (j>=size/2) ? Message( TO_RIGHT, CMD_EMPTY) :  
 //                       getState( getChild( p, !vs.swapRL ? 1 : 0 ) ).message;

 //       MessageBody msg_rb= msg_r.read( TO_PARENT ); 

 //       vs.doStep( msg_pb, msg_lb, msg_rb );
 //       if (  (j>=size/2) && vs.n->value != VK_EMPTY ){
 //           shortage= true;
 //       }

 //   }
    static Position initPos= Root;

    if ( history.size() >= MaxHistory )
        history.pop_front();
    history.push_back( stateMap );
    step( Root );
    day++;

    toOutside= getState( Position(1) ).message.read( TO_PARENT );

    StateMapType::iterator jj;
    for ( jj=stateMap.begin(); jj!=stateMap.end(); jj++){
        (*jj).second.commit();
    }

    bool delay= options::interact;
    do{
        cout << endl << "== " << day << " ==";
	    gState.printTree( cout, false );
        if ( options::showNet )
	        gState.print( cout, initPos );
        cout << flush;
        if ( options::interact ){
            char c= getch();
            switch( c ){
            case 'w': initPos= initPos>1 ? initPos/2 : initPos;   break;//   up
            case 'd':   initPos= initPos*2 +1;  break;//   right
            case 's':   initPos= initPos*2;     break;//   down
            case 'r':   initPos= 1;             break;//   root
            case '+':   
            case ' ':   delay= false;  break;
            case '-':   {
                if ( history.size()==0 )
                    break;
                day--;
                stateMap= history.back();
                history.pop_back();
                break;
                        }
            case 0x1b:   throw user_esc;
            default:
                wcout << hex << c << dec;
            }
        }
    }while( delay );
} 


//void
//GState::init()
//{
//    Position j;
//	for ( j=1; j<size; j++){
//		stateMap[ Position(j) ]= VState();
//        (stateMap[ Position(j) ]).n->x_dbg= j;
//    }
//}


void
VState::commit()
{
    VState* nn=n;
    *this=n[0];
    this->n = nn;
}

