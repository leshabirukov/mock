
#include "stdafx.h"

#ifdef _WIN32
#include <windows.h>
#endif

using namespace std;

void 
setConsoleColor ( int color )
{
#ifdef _WIN32
	static HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hConsole, color );
#else
    int color_x= (color &1)*4 + (color &2) + (color &4)/4;
//    printf( color&8 ? "\033[01;3%cm" : "\033[22;3%cm", (char)(color_x+'0') );
    static char code_light[]= "\033[01;30m";
    static char code_dark[]= "\033[22;30m";
    if ( color &8 ){
	code_light[6]= '0' +color_x;
        cout << code_light;
    }
    else{
	code_dark[6]= '0' +color_x;
        cout << code_dark;
    }
#endif
}

void 
printUsage()
{
    cout << "Combinatory logic computer simulator" << endl;
    cout << "Birukov A.A. 2014" << endl;
    cout << "usage:" << endl;
    cout << "mockingbird --program=<...> [--interact] ..." << endl << endl;
    cout << "--program=<...>    what to execute" << endl;
    cout << "--show-net         show 'computational hardware'" << endl;
    cout << "--interact         interactive mode" << endl << endl;
    cout << "Interactive mode keys:" << endl << endl;
    cout << "   <esc>           quit" << endl;
    cout << "   ' '             emulate one clock tick" << endl;
    cout << "   '-'             step one clock tick back" << endl;
    cout << "   w,s,d,r         navigate hardware net (requires --show-net):" << endl;
    cout << "     'w'           move up (root direction)" << endl;
    cout << "     'd'           move right" << endl;
    cout << "     's'           move left" << endl;
    cout << "     'r'           move to root" << endl;
    getch();
    cout << endl;
}

