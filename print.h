
class myStream
{
    size_t _pos;
    std::ostream& _stream;
public:
    myStream( std::ostream& s ) :_stream(s), _pos(0){}
    ~myStream(){    delete &_stream; }
    myStream& operator<<( char _Val ) 
    {   
        _stream << _Val; 
        _pos++;
        return *this; 
    }
    void put( char _Val, size_t place )
    {
        if ( _pos >place ){
            _stream << std::endl; 
            _pos= 0;
        }
        while ( _pos <place )
            *this << ' '; 
        *this << _Val; 
    }
    size_t pos() const {    return _pos; }
    std::ostream& levelDown() {    return _stream; }
};

extern myStream *stru;
