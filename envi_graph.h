

//    �������� ����, �� ������� ���� ���������
#pragma once

typedef unsigned long long Position;


enum VKind{     
                        VK_EMPTY= 0, 
                        VK_BOMB,
                        VK_EOF,
                            
                        VK_I0= 4,
                        VK_TRANSIT,
                        VK_APPLY,
                        
                        VK_K0= 8,
                        VK_K1,
                        
                        VK_S0= 12,
                        VK_S1,
                        VK_S2
};

enum VMeta{             
                        VMS_EMPTY=0, 
                        VMS_BUSY,
                        VMS_READY,
                        VMS_COPY,

                        VMS_APPLY,
                        VMS_PUMP,       //  pump right subtree to the left
                        VMS_IGNORE,     //  eat string from parent
                        VMS_WAIT        //  wait, for child finalize
        };

enum VComm{             CMD_EMPTY=0,    //  no command
                        CMD_BOMB,       //  erase own subtree
                        CMD_EOF,        //  ---
                        CMD_FINALIZE,   //  takes part in 2-nd parameter acquiring

                        CMD_COPY,       //  read own subtree as string and send to parent
                        CMD_APPLY,      //  apply string from parent to self

                                        //  send to left child and wait for responce, 
                        CMD_ECHO        //  to make proper delay before copying right child
        };

enum VTarget{             TO_PARENT=0, 
                        TO_LEFT,
                        TO_RIGHT,
                        TO_CHILDREN
        };


static inline int 
CH_NUM( VKind i )
{
    return i&3;
}

class MessageBody
{
protected:
    VKind k;
    VComm m;
    bool isk;
public:
    bool operator ==( const VKind& _k ){  return isk && k==_k;  }
    bool operator ==( const VComm& _m ){  return !isk && m==_m;  }
    MessageBody( VKind _k ):k(_k),isk(true){}
    MessageBody( VComm _m ):m(_m),isk(false){}
    MessageBody( const MessageBody& mb ):m(mb.m), k(mb.k), isk(mb.isk){}
    VKind getk() const { if (!isk && m<=CMD_EOF) return VKind(m); assert(isk); return k; }
    VComm getm() const { if (isk && k<=VK_EOF) return VComm(k); assert(!isk); return m; }
    bool zero() const { return (isk && k==VK_EMPTY)||(!isk && m==CMD_EMPTY); }
    bool is_bomb() const { return (!isk && m==CMD_BOMB); }
    bool is_eof() const { return ( isk && k==VK_EOF ); }
    friend char mnemonic( const MessageBody& msg );
    friend class GState; //void GState::printTree( ostream& ostr, bool showTransits, Position p, bool supressS1 );
};

class Message : public MessageBody
{
    VTarget targ;
public:
    Message( VTarget t, VKind _k ):MessageBody(_k), targ(t){}
    Message( VTarget t, VComm _m ):MessageBody(_m), targ(t){}
    Message( VTarget t, const MessageBody& mb ):MessageBody( mb ), targ(t){}
    MessageBody read( VTarget t )
    { 
        return targ==t || (targ== TO_CHILDREN && t!=TO_PARENT) ||
                (t== TO_CHILDREN && targ!=TO_PARENT) ? *this : MessageBody( VK_EMPTY );
    }
    friend char mnemonic( const MessageBody& msg );
    friend struct VState;
};

struct VState
{
    //  program node type: combinator, application form, or something 
    VKind value;
    //  what node doing now
    VMeta state;
    int step;
    //  message to the one of the neighbours
    Message message;

    //  if no children, means nothing,
    //  if one child - means it is on the right branch (default - left)
    //  if two children, means they swap branches
    bool swapRL;    

    static int ccc;

    VState():value(VK_EMPTY), state(VMS_EMPTY), message(TO_PARENT, VK_EMPTY), 
                                step(0),n(0),swapRL(false)
    { 
        n= new VState( *this, 0 ); 
        x_dbg=ccc++; 
    }
    VState( const VState& pro, int ):value(pro.value), state(pro.state), 
            message( pro.message ), step(pro.step), n(0),
            swapRL(pro.swapRL), x_dbg(pro.x_dbg)  {}

    VState( const VState& pro ):value(pro.value), state(pro.state), 
            message( pro.message ), step(pro.step), n(0),
            swapRL(pro.swapRL), x_dbg(pro.x_dbg)  
    {
        if ( pro.n )
            n= new VState( *(pro.n), 0 ); 
    }
    ~VState(){  if ( n )delete n;}
    void init( VKind );

    //  current node on the next step
    VState* n;
    void commit();
    void doStep( MessageBody& fromParent, 
                    MessageBody& fromLeft, MessageBody& fromRight );

    static char mnemonic( VKind );
    static char mnemonic_ms( VMeta );
    static char mnemonic_cmd( VComm );

	void print( std::ostream& ostr );
    //  for debug
    int x_dbg;
    int y_dbg;
};
    
typedef std::map< Position, VState > StateMapType;

class GState{
public:
    GState( int sz ):day(0), 
                    fromOutside( TO_CHILDREN, VK_EMPTY), toOutside( VK_EMPTY ){}
    void print( std::ostream& ostr, Position initPos=0 );
    void printTree( std::ostream& ostr, bool showTransits );
    void printTree( std::ostream& ostr, bool showTransits, Position p, bool supressS1 );
    VState& getState( Position p ){
        return stateMap[ p ];
    }
    void placeFromString( const char* prog )
    {
        if ( *placeFromString( getEntry() , prog ) !='\0' )
            throw "placeFromString: bad program string";
    }
    const char* placeFromString( Position p, const char* prog );
    void step( Position p );
    void step();
    Position getChild( Position p, size_t );
    Position getParent( Position p );
    static bool isRight( const Position p ){  return int(p)&1;    }
    static const Position Root= Position(1);
    bool isTerminal( const Position p ){  return ( p >= (1ull<<(sizeof(Position)*8-3)) );    }
    static Position getEntry(){ return Position(1); }
    Message fromOutside;
    MessageBody toOutside;
private:
    StateMapType stateMap;
    static const int MaxHistory= 10;
    std::deque< StateMapType > history;
    int day;
};

namespace options
{
    extern bool showNet;
    extern bool interact;
}

#define FOREGROUND_BLUE      0x0001 // text color contains blue.
#define FOREGROUND_GREEN     0x0002 // text color contains green.
#define FOREGROUND_RED       0x0004 // text color contains red.
#define FOREGROUND_INTENSITY 0x0008 // text color is intensified.
#define BACKGROUND_BLUE      0x0010 // background color contains blue.
#define BACKGROUND_GREEN     0x0020 // background color contains green.
#define BACKGROUND_RED       0x0040 // background color contains red.
#define BACKGROUND_INTENSITY 0x0080 // background color is intensified.
void setConsoleColor ( int color );

struct UserStopEsc{};

