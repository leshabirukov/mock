
#include "stdafx.h"
#include "lang_builtin.h"
#include "statistics.h"

using namespace std;

std::map< int, int> applyCount;

template< class T >void 
Curried<T>::visitAll( VisitorType v )
{
    size_t i;
    v( this );
    for ( i=0; i<args.size(); i++ ){
        if ( args[i] )
            args[i]->visitAll( v );
    }
}

template< class T >
void incrApllies<T>( Functor<T>* f, Functor<T>* arg, int slot )
{
	int iaInd= IsApplied<T>( f->getSrcCharNum(), arg->getSrcCharNum(), slot );
	applyCount[iaInd]= applyCount[iaInd]+1;
}

extern string prog;

string progAt( int n )
{
	string ret;
	char c= prog.at( n );
	ret.push_back( c );
	if ( c=='.' )
		ret.push_back( prog.at( n+1 ) );
	return ret;
}

void printApplyStats()
{
	map< int, int>::const_iterator b, e;
	int old= 0;
	for ( b=applyCount.begin(); b!=applyCount.end(); b++){
		int code= (*b).first;
		int count= (*b).second;
		int slot= (code>>12) & 0xff;
		int chNum= ((code>>20) & 0xfff);
		if (old != chNum)
			cout << endl;
		old= chNum;
		cout << chNum << " (" << progAt(chNum) << ") : ";
		chNum= (code & 0xfff);
		cout << chNum << " (" << progAt(chNum) << ") : ";
		cout << (slot) << " -> ";
		cout << (count) << endl;
	}
}

template void incrApllies<struct FunctorType1>( Functor<struct FunctorType1>* f, Functor<struct FunctorType1>* arg, int slot );
template void Curried<struct FunctorType1>::visitAll( Functor<struct FunctorType1>::VisitorType v );

template void incrApllies<struct FTypeNoRefs>( Functor<struct FTypeNoRefs>* f, Functor<struct FTypeNoRefs>* arg, int slot );
template void Curried<struct FTypeNoRefs>::visitAll( Functor<struct FTypeNoRefs>::VisitorType v );

